import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import VueRouter from 'vue-router'
import router from './routes'


Vue.use(VueRouter)
Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app');

Vue.filter('size', val => {
  var kbVal = Math.floor(val / 1024);
  if (kbVal >= 1024)
    return Math.floor(kbVal / 1024) + " MB";
  else
    return kbVal + " kB"
});