import VueRouter from 'vue-router'

export default new VueRouter({
    mode: 'history',
    base: 'vuefront',
    routes: [
        {
            path: '/',
            name: 'home',
            component: () => import('./components/Auth.vue')
        },
        
        {
            path: '/archivos',
            name: 'archivos',
            component: () => import('./components/Home.vue')
        },
        {
            path: '/chat',
            name: 'chat',
            component: () => import('./components/Chat.vue')
        }
    ]
})