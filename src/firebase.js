import firebase from '@firebase/app'

require('firebase/auth');
require('firebase/firestore');

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyBlAl3pDrzhGcTdmGaPhN3K9BpkWacEFfA",
    authDomain: "tercercorte-ddf23.firebaseapp.com",
    databaseURL: "https://tercercorte-ddf23.firebaseio.com",
    projectId: "tercercorte-ddf23",
    storageBucket: "tercercorte-ddf23.appspot.com",
    messagingSenderId: "473804871054",
    appId: "1:473804871054:web:e0c1f34a939ee5baf2c0df",
    measurementId: "G-YV8BXHWTVH"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

   const auth = firebase.auth()
   const db = firebase.firestore()

  export { auth, db };